execute pathogen#infect()
syntax on
set number
filetype plugin indent on
autocmd VimEnter * NERDTree
map <C-n> :NERDTreeToggle<CR>
" Unbind the cursor keys in insert, normal and visual modes.
for prefix in ['i', 'n', 'v']
  for key in ['<Up>', '<Down>', '<Left>', '<Right>']
      exe prefix . "noremap " . key . " <Nop>"
  endfor
endfor

colorscheme inkpot
set background=dark
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
